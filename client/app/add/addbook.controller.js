(function(){
    
        angular
            .module("BookCatApp")
            .controller("AddBookCtrl", ["$state", "BookApiSvc", AddBookCtrl ]);
    
        function AddBookCtrl($state, BookApiSvc){
            var addBookCtrl = this;
            
            var checkNoPhoto = "/nophoto/";
            addBookCtrl.queryStr = "";
            addBookCtrl.defaultBookCoverPath = "../assets/images/defaultbookcover.png";

            var initValues = function (){
                var lenOfResult = 0;
                var bookId = 0;
                var searchResult = [];
                var image_url = "";
                addBookCtrl.searchResultCount = 0;
                addBookCtrl.selectedAll = false;
                addBookCtrl.booksGoodReads = [];
                addBookCtrl.booksSelect = [];
                addBookCtrl.addFlag = [];
            }
            
            initValues();

            addBookCtrl.search = function(){
                initValues();
                BookApiSvc.searchBook(addBookCtrl.queryStr)
                .then(function(result){
                    //console.log("Results End >>> " + result.data[0]["total-results"]);
                    addBookCtrl.searchResultCount = result.data[0]["total-results"];
                    if (addBookCtrl.searchResultCount > 0) {
                        searchResult = result.data[0].results[0].work;
                        
                        lenOfResult = searchResult.length;
                        for(var i = 0; i < lenOfResult; i++){
                            //console.log("i = %d --> Rating: %s", i , addBookCtrl.booksGoodReads[i].average_rating[0]);
                            bookId = searchResult[i].best_book[0].id[0]._;
                            //console.log("bookId >>>" +  bookId);
    
                            //GET https://www.goodreads.com/book/show.xml?key={developer_key}&id={book_id}
                            BookApiSvc.getBookInfo(bookId)
                                .then(function(resultBookInfo){
                                    /*
                                        call a function to check is the resultBookInfo exist in the 
                                        database, table: books, books_user using isbn as reference
                                        if yes, addBookCtrl.addFlag.push = true, 
                                        else addBookCtrl.addFlag.push = false
                                    */
                                    // Check resultBookInfo.data.book[0].image_url[0]
                                    image_url = resultBookInfo.data.book[0].image_url[0];
                                    if (image_url.indexOf(checkNoPhoto) >= 0 ){
                                        resultBookInfo.data.book[0].image_url[0] = addBookCtrl.defaultBookCoverPath;
                                    }

                                    if( resultBookInfo.data.book[0].isbn[0] != "") {
                                        addBookCtrl.booksGoodReads.push(resultBookInfo.data.book[0]) ;
                                    }
                                        
                                    console.log(addBookCtrl.booksGoodReads);
                                }).catch(function(err){
                                    console.log(err);
                                })
                            }
                    } else { // lenOfResult <0
                        console.log("No Result");
                        initValues();
                        //console.log(result);
                    }
                    
                }).catch(function (err){
                    console.error(err);
                })
            }; //addBookCtrl.search
            
            addBookCtrl.selectAll = function(){
                console.log("Select all checked/unchecked");
                if (addBookCtrl.selectedAll){
                    addBookCtrl.selectedAll = true;
                } else {
                    addBookCtrl.selectedAll = false;
                }
                angular.forEach(addBookCtrl.booksGoodReads, function (item) {
                    item.Selected = addBookCtrl.selectedAll;
                });
            } // addBookCtrl.selectAll
            
            addBookCtrl.addBooks = function(){
                var i = 0;
                console.log("addBooks button clicked");
                angular.forEach(addBookCtrl.booksGoodReads, function (item) {
                    /*
                    console.log(i + "," + item.Selected);
                    i++;
                    */
                    /*
                        if addBookCtrl.addFlag[i] == false then call a function to add the selected books
                        else skip
                        i++
                    */
                });
            } // addBookCtrl.addBooks

            addBookCtrl.addBook = function(index){
                console.log("addBook button clicked");
                console.log("Clicked %d", index, ", %s ", addBookCtrl.booksGoodReads[index].title[0]);
                /*
                    call function to insert the book info into database
                    
                */
            } // addBookCtrl.addBook

        };  //AddBookCtrl
    
})();