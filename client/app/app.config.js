(function () {
    angular
        .module("BookCatApp")
        .config(BookCatAppConfig);
        BookCatAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    
    function BookCatAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/signIn.html"
                    }
                },
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "content": {
                        templateUrl: "../app/users/register.html"
                    }
                },
                controller: 'RegisterCtrl',
                controllerAs: 'ctrl'
            })
            .state("ResetPassword", {
                url: "/ResetPassword",
                views: {
                    "content": {
                        templateUrl: "../app/users/reset-password.html"
                    }
                },
                controller: 'ResetPasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state("ChangeNewpassword", {
                url: "/changeNewpassword?token",
                views: {
                    "content": {
                        templateUrl: "../app/users/change-new-password.html"
                    }
                },
                controller: 'ChangeNewPasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state("MyAccount", {
                url: "/MyAccount",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isLoggedIn());
                        return AuthService.isLoggedIn();
                    }
                },
                controller: 'MyAccountCtrl',
                controllerAs: 'ctrl'
            })
            .state("ChangePassword", {
                url: "/ChangePassword",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/changePassword.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isLoggedIn());
                        return AuthService.isLoggedIn();
                    }
                },
                controller: 'ChangePasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state('bookCat', {
                url: '/bookcat',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/index.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(√.isLoggedIn());
                        return AuthService.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('aboutus', {
                url: '/aboutus',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/aboutus.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isLoggedIn());
                        return AuthService.isLoggedIn();
                    }
                },
                controller: 'AboutUsCtrl',
                controllerAs: 'ctrl'
            })
            .state('guests', {
                url: '/guests',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/guest.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.isLoggedIn());
                        return AuthService.isLoggedIn();
                    }
                },
                controller: 'GuestListCtrl',
                controllerAs: 'ctrl'
            })
            .state('seatings', {
                url: '/seatings',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/seatings.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'SeatingsCtrl',
                controllerAs: 'ctrl'
            })
            .state('profile', {
                url: '/profile',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('back', {
                url: '/back',
                templateUrl: './app/users/login.html',
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/signIn");


    }
})();
