(function(){
    angular.module("BookCatApp")
    .service("BookApiSvc", ["$http","$q",BookApiSvc]);

    function BookApiSvc($http, $q){
        var bookApiSvc = this;

        // searchBook function
        bookApiSvc.searchBook = function(searchStr) {
            var defer = $q.defer();
            console.log (">>>> in Services, %s", searchStr)
            $http.get("/api/searchbook/" + searchStr, {
            }).then(function(result){
                    //console.log(result);
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
            
        }; // searchBook function

        // getBookInfo function
        bookApiSvc.getBookInfo = function (id){
            var defer = $q.defer();
            $http.get("/api/getbookinfo/" + id, {
            }).then(function(result){
                    //console.log(result);
                    defer.resolve(result);
                }).catch(function(err){
                    defer.reject(err);
                })
            return defer.promise;
        }; // getBookInfo function


    } //BookApiSvc
}) ();

