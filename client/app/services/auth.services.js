(function () {
    angular.module("BookCatApp")
    .service("AuthService", [
        "$http",
        "$q",
        AuthService
    ]);

    function AuthService($http, $q) {
        var authService = this;
        var user = null;
        authService.register = function(){

        }

        authService.login = function(userProfile){
            console.log(userProfile);
            var deferred = $q.defer();
            $http.post("/security/login", userProfile)
                .then(function(data){
                    console.log(data);
                    if(data.status == 200){
                        deferred.resolve();
                    }
                }).catch(function (error){
                    console.log(error);
                    user = false;
                    deferred.reject();
                })
            return deferred.promise;
        }

        authService.isUserLoggedIn = function(cb){
            $http.get("/status/user").then(function(data){
                console.log(data);
                user = true;
                cb(user);
            }).catch(function(error){
                console.log(error);
                user= false;
                cb(user);
            })
        }

        authService.logout = function(){
            var deferred = $q.defer();
            $http.get("/logout")
                .then(function(data){
                    console.log(data);
                    if(data.status == 200){
                        deferred.resolve();
                    }
                }).catch(function (error){
                    console.log(error);
                    user = false;
                    deferred.reject();
                })
            return deferred.promise;
        }
    }
})();   