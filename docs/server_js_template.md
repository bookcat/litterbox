//Load the libs
const q = require("q");
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);
const express = require("express");

const sessionStore = new MySQLStore({
	host: "localhost", port: 3306,
	user: "user", password: "user",
	database: "bootcat",
});

const pool = mysql.createPool({
	host: "localhost", port: 3306,
	user: "user", password: "user",
	database: "bookcat",
	connectionLimit: 4
});
const sessionConfig = session({
	secret: "secretkey",
	resave: true,
	saveUninitialized: true,
	store: sessionStore,
	cookie: {
		maxAge: 15 * 60 * 1000 //15 mins in ms
	}
});

const mkQuery = function(sql, pool) {
	return (function() {
		const defer = q.defer();
		//Collect arguments
		const args = [];
		for (var i in arguments)
			args.push(arguments[i]);

		pool.getConnection(function(err, conn) {
			if (err) {
				defer.reject(err);
				return;
			}

			conn.query(sql, args, function(err, result) {
				if (err) 
					defer.reject(err);
				else
					defer.resolve(result);
				conn.release();
			});
		});

		return (defer.promise);
	});
}



//Create an instance of the application
app = express();

app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());
app.use(sessionConfig);

app.use(function(req, resp, next) {

	if (!req.session.queryCache) {
		console.log("initializing new session");
		req.session.queryCache = {};
	}
	next();
});

app.use("/libs", express.static(path.join(__dirname, "../bower_components")));
app.use(express.static(path.join(__dirname, "../client")));

const port = process.env.APP_PORT || 3000;

app.listen(port, function() {
	console.log("Application started at %s on port %d"
			, new Date(), port);
});
