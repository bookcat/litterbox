'use strict';

var ENV = process.env.NODE_ENV || "development";
//console.log("ENV = %s", ENV);

module.exports = require('./' + ENV + '.js') || {};