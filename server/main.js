//Load the libs
const q = require("q");
const fs = require('fs');
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);
const express = require("express");

const Sequelize = require("sequelize");
const passport   = require('passport');  //to handle authentication

var app = express();
//console.log (__dirname);
// app.use("/libs", express.static(path.join(__dirname, "../client/bower_components")));
// app.use(express.static(path.join(__dirname, "../client")));

// bodyParser
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

// Initialize session
app.use(session({
    secret: "a4357200bfaf6f64cca3655028b157c5ea2e293f7d4a5559b61593c8e50f6168",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

var config = require("./config");
var db = require("./database");

require('./auth')(app, passport);
require('./routes')(app, db, passport);

app.listen(config.port, function () {
  console.log("Server running at http://localhost:" + config.port);
});