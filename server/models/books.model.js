var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('books', {
        bookid: {
            type : Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        isbn10: {
            type:Sequelize.STRING,
            allowNull: false
        },
        isbn13: {
            type:Sequelize.STRING,
            allowNull: false
        },
        title: {
            type:Sequelize.STRING,
            allowNull: false
        },
        author: {
            type: Sequelize.STRING,
            allowNull: false
        },
        format: {
            type:Sequelize.string,
            allowNull: true
        },
        imgurl: {
            type: Sequelize.string,
            allowNull: true
        },
        synopsis: {
            type: Sequelize.STRING(2000),
            allowNull: true
        }
        // ,
        // createdat: {
        //     type: Sequelize.DATE,
        //     allowNull: false
        // },
        // updatedat: {
        //     type: Sequelize.DATE,
        //     allowNull: true
        // },
        // deletedat: {
        //     type: Sequelize.DATE,
        //     allowNull: true
        // }
    });
};