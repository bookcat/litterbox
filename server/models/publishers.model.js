var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('roles', {
        pubid: {
            type : Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        publisherame: {
            type:Sequelize.STRING,
            allowNull: false
        }
    });
};