var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('roles', {
        roleid: {
            type : Sequelize.INTEGER(2),
            allowNull: false,
            primaryKey: true,
        },
        rolename: {
            type:Sequelize.ENUM('admin','user','disabled'),
            defaultValue: 'user',
            allowNull: false
        },
        desc: {
            type:Sequelize.STRING,
            allowNull: true
        }
    });
};