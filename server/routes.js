"use strict";

const request = require('request');
const parseString = require('xml2js').parseString;
const path = require("path");
const express = require("express");
const config = require("./config");

module.exports = function(app, database, passport){
    /* declare const */
    //GOOREADS API
    const GOODREADS_SEARCH_URL = "https://www.goodreads.com/search";
    const GOODREADS_BOOKS_URL = "https://www.goodreads.com/book/show.xml";
    const GOODREADS_KEY = "vqR2AIjf0ZTjzE8rq9rEA";

    const HOME_PAGE = "/index.html#!/home";
    const SIGNIN_PAGE = "/index.html#!/login";
    const CLIENT_FOLDER = path.join(__dirname + '/../client');
    const LIBS_FOLDER = path.join(CLIENT_FOLDER + "/bower_components");
    //console.log (">>>>> LIBS_FOLDER >>>>> %s",LIBS_FOLDER);
    app.use("/libs", express.static(LIBS_FOLDER));
    app.use(express.static(CLIENT_FOLDER));
    
    app.get("/api/searchbook/:searchStr", function(req, resp) {
        //console.log(">>>>>Search Str >>> %s", req.params.searchStr);
        
        //request('https://www.goodreads.com/search?key=vqR2AIjf0ZTjzE8rq9rEA&q=' + req.params.searchStr
        var goodReadsRequest = GOODREADS_SEARCH_URL + "?key=" + GOODREADS_KEY + "&q=" + req.params.searchStr;
      
        request(goodReadsRequest, 
          function (error, response, body) {
            //console.log('body:', body); 
            parseString(body, function (err, result) {
                //console.log(JSON.stringify(result.GoodreadsResponse.search));
                resp.json(result.GoodreadsResponse.search);
            });
          });
      });
      
      app.get("/api/getbookinfo/:id", function(req, resp) {
        
        //request('https://www.goodreads.com/search?key=vqR2AIjf0ZTjzE8rq9rEA&q=' + req.params.searchStr
        var goodReadsRequest = GOODREADS_BOOKS_URL + "?key=" + GOODREADS_KEY + "&id=" + req.params.id;
      
        //console.log(goodReadsRequest);
        request(goodReadsRequest, 
          function (error, response, body) {
            //console.log('body:', body); 
            parseString(body, function (err, result) {
                //console.log(JSON.stringify(result.GoodreadsResponse.search));
                resp.json(result.GoodreadsResponse);
            });
          });
      });

    app.post("/security/login", passport.authenticate("local", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/status/user", function(req, res){
        var status = null;
        if(req.user){
            status = req.user;
        }

        res.status(200).json(status);
    });

    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('..' + HOME_PAGE);
    });

    // Facebook Authentication
    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/logout", function(req, res) {
        req.logout(); // clears the passport session
        req.session.destroy(); // destroys all session related data
        res.send(req.user).end();
    });

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(SIGNIN_PAGE);
    }

    app.use(function(req, res, next) {
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });

};